class VisibilityToggle extends React.Component {
    constructor(props){
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            v: '',
            x: 'Show Details'
        }
    }
    handleToggleVisibility(){
        this.setState((prevState) => {
            return {
                v: (prevState.v == 'Visible') ? '':'Visible',
                x: (prevState.x == 'Show details') ? 'Hide details':'Show details'
            };
        });
    }
    render(){
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>
                    {this.state.x}
                </button>
                <p>{this.state.v}</p>
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById('app'));