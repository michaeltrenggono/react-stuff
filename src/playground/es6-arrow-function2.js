const add = (a, b) => {
    // console.log(arguments);
    return a + b;
};

console.log(add(55,7));

const user = {
    name: 'Michael',
    cities: ['Jakarta', 'Melbourne'],
    printPlacesLived(){
        return this.cities.map((city) => this.name + ' has lived in ' + city);
    } 
}

console.log(user.printPlacesLived());

const multiplier = {
    numbers: [8,9,6],
    multiplyBy: 3,
    multiply(){ 
        return this.numbers.map((number) => number * this.multiplyBy);
    }
}

console.log(multiplier.multiply());